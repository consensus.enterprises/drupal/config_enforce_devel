<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce_devel\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Theme settings form tests.
 *
 * @group config_enforce_devel
 */
class ThemeSettingsFormTest extends BrowserTestBase {

  /**
   * The admin user used in this test.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_enforce', 'config_enforce_devel', 'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer themes',
    ]);

  }

  /**
   * Test that the theme settings form can be submitted without errors.
   *
   * @see https://www.drupal.org/project/config_enforce_devel/issues/3355729
   *   Intended to catch regressions of this issue.
   *
   * @see \Drupal\system\Form\ThemeSettingsForm::submitForm()
   *   This passes most top-level form values to
   *   \theme_settings_convert_to_config(), which then attempts to set them all
   *   on the configuration object it's passed. This is a problem for us because
   *   the dot character ('.') has a special meaning in Drupal's configuration
   *   system and will throw an error when it tries to set
   *
   * @see \theme_settings_convert_to_config()
   *   Called from \Drupal\system\Form\ThemeSettingsForm::submitForm() and
   *   triggers fatal error.
   *
   * @see \Drupal\config_enforce_devel\Form\EmbeddedEnforceForm::buildForm()
   *   We add our Config Enforce form elements in this and various methods it
   *   calls.
   */
  public function testThemeSettingsFormSubmit(): void {

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/appearance/settings/stark');

    $this->submitForm([], 'Save configuration');

    $this->assertSession()->pageTextNotContains(
      'Drupal\Core\Config\ConfigValueException: stark.settings key contains a dot which is not supported.'
    );

    /** @var int The HTTP status code of the form submission response. */
    $statusCode = $this->getSession()->getStatusCode();

    // On the off chance that the assert above doesn't find the
    // ConfigValueException text but a fatal error was nonetheless thrown,
    // assert that the HTTP status code of the response is expected to be in
    // the 1-399 range, which is the non-error range; if the response code is
    // 400 or higher, it's an error. The error code we expect for this
    // particular bug is 500, but casting a wide net here will allow us to
    // potentially catch other unexpected errors.
    //
    // @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
    $this->assertSession()->assert($statusCode < 400, \sprintf(
      'Expected a successful HTTP status code but got code "%s".',
      $statusCode
    ));

  }

}
