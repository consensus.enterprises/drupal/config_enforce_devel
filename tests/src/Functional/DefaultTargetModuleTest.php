<?php

declare(strict_types=1);

namespace Drupal\Tests\config_enforce_devel\Functional;

use Drupal\config_enforce_devel\TargetModuleCollection;
use Drupal\Tests\BrowserTestBase;

/**
 * Default target module tests.
 *
 * @group config_enforce_devel
 */
class DefaultTargetModuleTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['config_enforce_devel'];

  /**
   * Test that the default target module is successfully created and installed.
   *
   * Note that this only tests that TargetModuleCollection correctly creates and
   * installs the default target module by calling its method directly, and does
   * not test whether visiting the enforced configs admin page triggers this.
   */
  public function testDefaultTargetModuleCreated(): void {

    /** @var \Drupal\config_enforce_devel\TargetModuleCollection */
    $targetModuleCollection = new TargetModuleCollection();

    $targetModuleCollection->ensureDefaultTargetModuleIsSet();

    /** @var \Drupal\Core\Extension\Extension */
    $this->container->get('module_handler')->getModule(
      $targetModuleCollection->getDefaultTargetModuleName()
    );

    // The Drupal module handler service will throw
    // \Drupal\Core\Extension\Exception\UnknownExtensionException if attempting
    // to get the Extension object of a module that doesn't exist. If an
    // exception was not thrown and this assertion runs, that means the module
    // does exist.
    $this->assertTrue(true);

  }

}
