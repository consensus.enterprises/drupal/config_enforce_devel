@settings @api
Feature: Enforce Basic site settings.
  In order to enforce configs on Basic site settings
  As an Administrator
  I need to be able to deny updates to Basic site settings.

  Background:
    Given I run 'cd ..; make dev'

  Scenario:
    Given I am logged in as an "Admin"
     When I go to "admin/config/system/site-information"
      And I check the box "Enforce config"
      And I press "Save configuration"
     Then I should see "The configuration options have been saved."
     When I go to "admin/config/system/cron"
      And I check the box "Enforce config"
      And I press "Save configuration"
     Then I should see "The configuration options have been saved."
     When I go to "admin/config/development/config_enforce/enforced_configs"
     Then I should see "system.site"
      And I should see "system.cron"
     When I check "edit-enforced-configs-systemsite"
      And I check "edit-enforced-configs-systemcron"
      And I press "edit-delete-enforced-configs-submit"
# @TODO message selectors appear not to work.
# See behat.yml in config-enforce-localdev.
#     Then I should see the success message "Deleted enforcement settings for:"
     Then I should see "Deleted enforcement settings for:"
#      And I should see the success message containing "system.cron"
      And I should see "system.cron"
#      And I should see the success message containing "system.site"
      And I should see "system.site"
     When I reload the page
     Then I should not see "system.site"
      And I should not see "system.cron"

