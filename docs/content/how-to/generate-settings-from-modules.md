---
title: How to generate enforcement settings from modules
menuTitle: Generate settings from modules
weight: 20

---

From the main *Enforced Configs* admin screen, the *Generate from modules*
button will provide a list of all enabled Target modules, and nested lists of
all configuration objects exported to them.

Select an Enforcement level for the set of configuration objects you will
enforce. Select each of your Target modules in turn, and when selected, they
will expand to show the config directory (usually `optional` or `install`), and
finally the individual configuration objects within them.

Once you have selected everything you want to enforce, click Save. Config
Enforce Devel will write entries for each into a registry for the appropriate
target module, using the Enforcement level you selected. These config objects
are now enforced, and you can continue your development work.

