---
title: How to add Config Enforce to an existing project
menuTitle: Add Config Enforce to a project
weight: 10

---

The first time you enable `config_enforce_devel` and go to *Enforced Configs*
(*admin/config/development/config_enforce/enforced_configs*), it will generate
a new `config_enforce_default` module to be the default target for enforced
configs. This is a convenience for bootstrapping a new Config Enforce setup. In
the case of an existing site, you probably already have some modules containing
configuration you'd like to enforce, and don't need the default target module.

To enable your existing modules, and remove the Config Enforce Default target,
follow these steps:

1. Go to *Enforced Configs > Settings* (*admin/config/development/config_enforce/settings*)
2. In the *Target modules* fieldset, select any of your existing modules and/or
   install profile containing exported configuration objects you want to enforce. Save configuration.
3. In the *Enforcement field defaults* fieldset, and select a different
   default Target module than *Config Enforce Default*. Save configuration.
4. Go back to the *Target modules* fieldset, and remove the *Config Enforce
   Default* module from the selected options. Save configuration.
5. Enforce these `config_enforce.devel` settings by checking the *Enforce
   Config* box in the Config Enforce "floaty window" UI. These should live in a
   `site_devel` target module which is only be turned on in development environments.
6. You can now remove the *Config Enforce Defaults* module from the
   *DRUPAL_ROOT/modules/custom* folder.

At this point Config Enforce Devel is configured with your existing set of
configuration-containing modules as targets for enforced configs. All that
remains is to generate enforcement settings for these exported configuration
objects.

To do so, there are two options: either a) generate enforcement settings from
the existing modules on disk, or b) generate enforcement settings by selecting
configuration in the active config (database) for the site.


