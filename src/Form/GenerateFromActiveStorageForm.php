<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\config_enforce_devel\EnforcedConfig;
use Drupal\config_enforce_devel\Form\GenerateFormBase;

/**
 * Select which config objects to generate settings for.
 */
class GenerateFromActiveStorageForm extends GenerateFormBase {

  const FORM_ID = 'config_enforce_devel_generate_from_active_storage_form';
  const MODAL_TITLE = 'Generate from active storage';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    parent::buildForm($form, $form_state);

    $this->addTargetModuleField();
    $this->addConfigDirectoryField();
    $this->addEnforcementLevelField();
    $this->addActiveStorageConfigs();

    return $this->form();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormName() {
    return 'config_enforce_devel_generate_from_active_storage_confirm_form';
  }

  /**
   * Add field to select one or more config objects to enforece.
   */
  protected function addActiveStorageConfigs() {
    $this->form()['generate']['active_storage'] = [
      '#type' => 'multiselect',
      '#title' => $this->t('Select active storage configs'),
      '#description' => $this->t('These config objects will have config enforcement settings generated for them. '),
      '#options' => $this->getUnenforcedConfigs(),
      '#multiple' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfigsToGenerate() {
    $generate = $this->formState()->getValue('generate');
    $enforced_configs = [];
    foreach ($generate['active_storage'] as $config_name) {
      $enforced_configs[$config_name] = [
        'target_module' => $generate['target_module'],
        'config_directory' => $generate['config_directory'],
        'enforcement_level' => $generate['enforcement_level'],
      ];
    }
    return $enforced_configs;
  }

}
