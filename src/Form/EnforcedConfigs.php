<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Tableselect;
use Drupal\Core\Url;
use Drupal\config_enforce\EnforcedConfig;
use Drupal\config_enforce_devel\Form\DevelFormHelperTrait;

/**
 * Form that lists all the enforced configs, and allows them to be updated.
 */
class EnforcedConfigs extends FormBase {

  use DevelFormHelperTrait;

  const FORM_ID = 'config_enforce_enforced_configs_form';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    $this->addUpdateEnforcedConfigs();
    $this->addDeleteEnforcedConfigs();
    $this->addEnforcedConfigsTable();

    $this->form()['#attached']['library'][] = 'config_enforce_devel/config-enforce-devel';
    $this->form()['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $this->form();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    if (!$this->triggerIsUpdateEnforcedConfigsButton()) return;

    if (empty($this->getSelectedConfigs())) {
      $this->formState()
        ->setErrorByName('enforced_configs', $this->t('At least one enforced config must be selected.'));
    }
    if (empty($this->getUpdatedSettings())) {
      $this->formState()
        ->setErrorByName('update_enforced_configs][updated_settings', $this->t('At least one setting must be updated.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedFormProperties($form, $form_state);

    if ($this->triggerIsUpdateEnforcedConfigsButton()) return $this->updateEnforcedConfigs();
    if ($this->triggerIsDeleteEnforcedConfigsButton()) return $this->deleteEnforcedConfigs();
  }

  /**
   * Update selected enforced configs with new settings.
   */
  protected function updateEnforcedConfigs() {
    $enforced_configs_to_update = $this->getEnforcedConfigsToUpdate();
    $this->getEnforcedConfigCollection()->UpdateEnforcedConfigs($enforced_configs_to_update);

    $this->messenger()->addMessage(
      $this->t('Updated enforcement settings for: %config_list', [
        '%config_list' => $this->renderHtmlList(array_keys($enforced_configs_to_update)),
      ])
    );
  }

  /**
   * Delete selected enforced config settings.
   */
  protected function deleteEnforcedConfigs() {
    $enforced_configs_to_delete = $this->getEnforcedConfigsToDelete();
    $this->getEnforcedConfigCollection()->deleteEnforcedConfigs($enforced_configs_to_delete);

    $this->messenger()->addMessage(
      $this->t('Deleted enforcement settings for: %config_list', [
        '%config_list' => $this->renderHtmlList(array_keys($enforced_configs_to_delete)),
      ])
    );
  }

  /**
   * Return an array of enforced configs to update.
   */
  protected function getEnforcedConfigsToUpdate() {
    $enforced_configs = $this->getEnforcedConfigCollection()->getEnforcedConfigs();
    $enforced_configs_to_update = [];
    $updated_settings = $this->getUpdatedSettings();

    foreach ($this->getSelectedConfigs() as $config_name) {
      $enforced_configs_to_update[$config_name] = array_merge($enforced_configs[$config_name], $updated_settings);
    }

    return $enforced_configs_to_update;
  }

  /**
   * Return an array of enforced configs to delete.
   */
  protected function getEnforcedConfigsToDelete() {
    $enforced_configs = $this->getEnforcedConfigCollection()->getEnforcedConfigs();
    $enforced_configs_to_delete = [];
    foreach ($this->getSelectedConfigs() as $config_name) {
      $enforced_configs_to_delete[$config_name] = $enforced_configs[$config_name];
    }
    return $enforced_configs_to_delete;
  }

  /**
   * Determine whether the form was submitted with the "Update settings" button.
   */
  protected function triggerIsUpdateEnforcedConfigsButton() {
    $button = $this->formState()->getTriggeringElement();
    // @TODO: There must be a better was to determine this.
    return $button['#value'] == "Update enforced configs";
  }

  /**
   * Determine whether the form was submitted with the "Update settings" button.
   */
  protected function triggerIsDeleteEnforcedConfigsButton() {
    $button = $this->formState()->getTriggeringElement();
    // @TODO: There must be a better was to determine this.
    return $button['#value'] == "Delete enforced configs";
  }

  /**
   * {@inheritdoc}
   *
   * This provides an easy way for enforced configs to be enforced themselves.
   */
  public function getEditableConfigNames() {
    return $this
      ->getEnforcedConfigCollection()
      ->getTargetModuleCollection()
      ->getRegistryConfigNames();
  }

  /**
   * Add fields to update config enforce settings for multiple enforced configs.
   */
  protected function addUpdateEnforcedConfigs() {
    $this->form()['update_enforced_configs'] = [
      '#type' => 'details',
      '#title' => 'Update enforced config settings',
      '#tree' => TRUE,
    ];
    $this->form()['update_enforced_configs']['updated_settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $this
      ->addUpdateTargetModule()
      ->addUpdateConfigDirectory()
      ->addUpdateEnforcementLevel();

    $this->form()['update_enforced_configs']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Update enforced configs',
      '#submit' => ['::submitForm'],
    ];
  }

  /**
   * Add field to update the target module for multiple enforced configs.
   */
  protected function addUpdateTargetModule() {
    $this->form()['update_enforced_configs']['updated_settings']['target_module'] = $this->getTargetModuleField();
    $this->form()['update_enforced_configs']['updated_settings']['target_module']['#options'] = ['no-op' => 'No change']
      + $this->form()['update_enforced_configs']['updated_settings']['target_module']['#options'];
    $this->form()['update_enforced_configs']['updated_settings']['target_module']['#default_value'] = 'no-op';
    return $this;
  }

  /**
   * Add fields to update the config directory for multiple enforced configs.
   */
  protected function addUpdateConfigDirectory() {
    $this->form()['update_enforced_configs']['updated_settings']['config_directory'] = $this->getConfigDirectoryField();
    $this->form()['update_enforced_configs']['updated_settings']['config_directory']['#options'] = ['no-op' => 'No change']
      + $this->form()['update_enforced_configs']['updated_settings']['config_directory']['#options'];
    $this->form()['update_enforced_configs']['updated_settings']['config_directory']['#default_value'] = 'no-op';
    return $this;
  }

  /**
   * Add fields to update the enforcement level for multiple enforced configs.
   */
  protected function addUpdateEnforcementLevel() {
    $this->form()['update_enforced_configs']['updated_settings']['enforcement_level'] = $this->getEnforcementLevelField();
    $this->form()['update_enforced_configs']['updated_settings']['enforcement_level']['#options'] = ['no-op' => 'No change']
      + $this->form()['update_enforced_configs']['updated_settings']['enforcement_level']['#options'];
    $this->form()['update_enforced_configs']['updated_settings']['enforcement_level']['#default_value'] = 'no-op';
    return $this;
  }

  /**
   * Add fields to update config enforce settings for multiple enforced configs.
   */
  protected function addDeleteEnforcedConfigs() {
    $this->form()['delete_enforced_configs'] = [
      '#type' => 'details',
      '#title' => 'Delete enforced configs',
      '#description' => 'Select enforced configs to delete. This will not delete the associated configuration objects; just the enforcements settings.',
      '#tree' => TRUE,
    ];

    $this->form()['delete_enforced_configs']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Delete enforced configs',
      '#submit' => ['::submitForm'],
    ];
  }

  /**
   * Add table to select multiple enforced configs to allow their settings to be updated.
   */
  protected function addEnforcedConfigsTable() {
    $this->form()['enforced_configs'] = [
      '#type' => 'tableselect',
      '#header' => $this->getHeader(),
      '#options' => $this->getOptions(),
      '#empty' => $this->t('No enforced config objects found'),
      // Disable `enforced_configs`, since they're read-only.
      // @TODO: Remove this once https://www.drupal.org/project/drupal/issues/2895352 lands.
      // @see processDisabledRows().
      '#process' => [
        // This is the original #process callback.
        [Tableselect::class, 'processTableselect'],
        // Additional #process callback.
        [static::class, 'processDisabledRows'],
      ],
    ];
  }

  /**
   * Disable checkboxes for appropriate rows.
   *
   * @TODO: Remove this once https://www.drupal.org/project/drupal/issues/2895352 lands.
   * @see addEnforcedConfigsTable().
   */
  public static function processDisabledRows(array &$element): array {
    foreach (Element::children($element) as $key) {
      $element[$key]['#disabled'] = isset($element['#options'][$key]['#disabled']) ? $element['#options'][$key]['#disabled'] : FALSE;
    }
    return $element;
  }

  /**
   * Return a table header render array.
   */
  protected function getHeader() {
    return [
      'config_name' => ['data' => $this->t('Configuration object name')],
      'enforcement_level' => ['data' => $this->t('Enforcement level')],
      'target_module' => ['data' => $this->t('Target module')],
      'config_directory' => ['data' => $this->t('Config directory')],
      'settings' => ['data' => $this->t('Config enforce settings')],
    ];
  }

  /**
   * Return a tableselect options render array.
   */
  protected function getOptions() {
    $rows = [];
    foreach ($this->getEnforcedConfigs() as $config_name) {
      $this->setCurrentConfig($config_name);

      $settings = [
        'data' => [
          '#type' => 'link',
          '#title' => $this->t('Settings'),
          '#url' => Url::fromRoute('config_enforce_devel.config_edit', ['arg' => $config_name]),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button',
            ],
          ],
        ],
      ];

      $rows[$config_name] = [
        '#attributes' => [
          'class' => $this->getRowClasses(),
        ],
        // Disable enforced config registry config objects since they're read-only.
        '#disabled' => $this->isRegistryConfig(),
        'config_name' => $this->getConfigNameLink(),
        'enforcement_level' => EnforcedConfig::getEnforcementLevelLabel($config_name),
        'target_module' => EnforcedConfig::getTargetModule($config_name),
        'config_directory' => EnforcedConfig::getConfigDirectory($config_name),
        'settings' => $settings,
      ];
    }

    return $rows;
  }

  /**
   * Return the config name as a link to its form, if the URI is available.
   */
  protected function getConfigNameLink() {
    $config_name = $this->getCurrentConfig();
    $uri = EnforcedConfig::getConfigFormUri($config_name);
    if (empty($uri)) return $config_name;
    return Link::fromTextAndUrl($config_name, Url::fromUri('internal:' . $uri));
  }

  /**
   * List only enforced config object names.
   */
  protected function getEnforcedConfigs() {
    $enforced = [];
    $enforced_configs = $this->getEnforcedConfigCollection()->getEnforcedConfigs();
    ksort($enforced_configs);
    foreach ($enforced_configs as $config_name => $setting) {
      $enforced[] = $config_name;
    }
    return $enforced;
  }

  /**
   * Return a list of selected enforced configs.
   */
  protected function getSelectedConfigs() {
    return array_filter($this->formState()->getValue('enforced_configs'));
  }

  /**
   * Return an array of settings to update and the updated values.
   */
  protected function getUpdatedSettings() {
    $updated_settings = $this->formState()->getValue('update_enforced_configs')['updated_settings'];
    foreach ($updated_settings as $setting => $value) {
      if ($value == 'no-op') {
        unset($updated_settings[$setting]);
      }
    }
    return $updated_settings;
  }

}
