<?php

declare(strict_types=1);

namespace Drupal\config_enforce_devel\EventSubscriber;

use Drupal\config_enforce_devel\EnforcedConfigCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;

/**
 * ConfigDeleteSubscriber subscriber for configuration CRUD events.
 */
class ConfigDeleteSubscriber implements EventSubscriberInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The enforced config collection.
   *
   * @var \Drupal\config_enforce_devel\EnforcedConfigCollection
   */
  protected $enforcedConfigCollection;

  /**
   * Constructs the ConfigDeleteSubscriber object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
    $this->enforcedConfigCollection = new EnforcedConfigCollection();
  }

  /**
   * React to configuration ConfigEvent::DELETE events.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The event to process.
   */
  public function onConfigDelete(ConfigCrudEvent $event) {
    $config_to_delete = $event->getConfig()->getName();
    $enforced_configs = $this->enforcedConfigCollection->getEnforcedConfigs();

    if (!array_key_exists($config_to_delete, $enforced_configs)) return;

    $enforced_configs_to_delete = [$config_to_delete => $enforced_configs[$config_to_delete]];
    $this->enforcedConfigCollection->deleteEnforcedConfigs($enforced_configs_to_delete);
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents() {
    $events[ConfigEvents::DELETE][] = array('onConfigDelete', 10);
    return $events;
  }

}
