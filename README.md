Config Enforce Devel
====================

This module provides a convenient development UI for [Config
Enforce](https://drupal.org/project/config_enforce).

This module should *only* be installed in development environments. See the
[Security
Model](https://config-enforce.consensus.enterprises/reference/security-model/)
documentation for details.

For more information see the [documentation
site](https://config-enforce.consensus.enterprises/) or the [project
page](https://www.drupal.org/project/config_enforce_devel)

